function Item(x,y,speed,type){
	this.x=x;
	this.y=y;
	this.speed=speed;
	this.type=type;  
	this.cnt=0;
	this.location=Math.floor((Math.random() * 100) + 1);//아이템 떨어지는 방향
	this.move=function(){
		this.cnt+=speed;
		if(this.cnt<=100){   
			this.y-=this.speed-2;   
			if(this.location%2==0){
				this.x+=this.speed-3;
			}else{ 
				this.x-=this.speed-3;
			}
		}else{
			if(this.cnt<=200){
				this.y+=this.speed-1;
			}else{
				this.y+=this.speed;
			}
		}
	}
	this.getX=function(){
		return this.x;
	}
	this.getY=function(){
		return this.y;
	}
	this.getType=function(){
		return this.type;
	}
	this.getSpeed=function(){
		return this.speed;
	}
}
