function Monster(x,y,speed,shield) {
	this.x=x;
	this.y=y;
	this.speed=speed;
	this.shield=shield;
	this.move=function(){
		this.y+=this.speed;
	}
	this.getX=function(){
		return this.x;
	}
	this.getY=function(){
		return this.y;
	} 
	this.getShield=function() {
		return this.shield;
	} 
	this.setHit=function(power){ 
		this.shield-=power; 
	}
	
	
}
