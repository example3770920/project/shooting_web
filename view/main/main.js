'use strict';

angular.module('myApp.main', ['ngRoute'])
.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/main', {
    templateUrl: 'view/main/index.html',
  });   
}])   
.controller('mainCtrl', function($scope,$http) {
	// animation globals
	var t=0;  
	var game=true; 
	var frameInterval = 10; // in ms
	var canvas = null; // canvas DOM object
	var context = null; // canvas context
	var score=0;  
	var canvas2 =null;	// buffer canvas
	var context2 =null;
	var backGround_y1=0; 
	var backGround_y2=-800;// 배경 좌표
	var user_Left=true;
	var user_Right=true;
	var user_shield=0;
	var user_x=0; 
	var user_y=450; // 사용자 좌표	  
	var missile_power=1;
	var userImg=null;
	var missile_img=null;
	var monster_img;// 적 이미지
	var monster_shield = 3;
	var monster_speed = 3; 
	var score_img;// 점수 이미지
	var boomImgArray=new Array();// 폭파 이미지
	var itemImgArray=new Array();//
	var itemArray=new Array();
	var boomArray=new Array();//	
	var groundArray=new Array();// 배경 이미지
	var missileArray=new Array();// 
	var monsterArray=new Array();// 
	var modal_title="";
	var thread;	
	$scope.gameReSet=function(){
		t=0;
		game=true;
		score=0;
		backGround_y1=0;
		backGround_y2=-800;
		user_Left=true;
		user_Right=true;
		user_shield=0;
		user_x=50;
		user_y=450;
		missile_power=1;
		monster_shield=3;
		monster_speed=3;
		init(); 
	}
	function init() { 
	    canvas=document.getElementById("myCanvas");
	    context=canvas.getContext("2d");
	    initStageObjects();
	    initEvent();
	    thread=setInterval(updateStage, frameInterval);
	}
	function initStageObjects() {
		canvas2=document.createElement('canvas');
		canvas2.width = 480;
		canvas2.height = 600;
		context2=canvas2.getContext('2d'); 
		var ground_img = new Image();
		var ground_img2=new Image();
		ground_img.src="/img/ground.png";
		ground_img2.src="/img/ground.png";
		groundArray.push(ground_img);
		groundArray.push(ground_img2);
		
		score_img=new Image();
		score_img.src="/img/score.png";
		
		userImg=new Image();  
		userImg.src="/img/user.png";
		missile_img=new Image();
		missile_img.src="/img/missile.png";
		
		var boom_img = new Image();
		var boom_img2=new Image();
		boom_img.src="/img/boom1.png";
		boom_img2.src="/img/boom2.png";
		boomImgArray.push(boom_img);
		boomImgArray.push(boom_img2);
		
		var item_img1 = new Image();
		var item_img2=new Image();
		var item_img3=new Image();
		item_img1.src="/img/item1.png";
		item_img2.src="/img/item2.png";
		item_img3.src="/img/item3.png";
		
		itemImgArray.push(item_img1);
		itemImgArray.push(item_img2);
		itemImgArray.push(item_img3);
		
		
		
	}
	function initEvent(){  
		$(window).keydown(function( event ) {
			if(event.keyCode==39){//right
				user_Right=true;
				user_Left=false;
				return;
			} 
			if(event.keyCode==37){
				user_Left=true;
				user_Right=false;
				return;
			}
		}); 
		$(window).keyup(function( event ) {
			user_Right=false;
			user_Left=false;
		}); 
	}
	function updateStage() {
		if(game){
		    clearCanvas();
		    updateStageObjects();//좌표수정
		    drawStageObjects(); //그리기
		    context.drawImage(canvas2,0,0); 
			if (user_shield == 1) {
				if(boomArray.length==0){
					modal_title="미션 실패";
					game=false;
				} 
			} 
		    t++; 
		}else{ 
			 clearInterval(thread); 
			 $('#myModal').on('shown.bs.modal', function () {
				 $("#modal_title").html(modal_title); 
				 $("#modal_score").html(score+"점");
			 });   
			 $('#myModal').modal('show');    
		} 
	}
	function clearCanvas() {
	    context.clearRect(0,0,canvas.width, canvas.height);
	}
	function updateStageObjects() { 
		gameUpdate();
		userUpdate();
		groundUpdate();
		monsterUpdate();
		missileUpdate();
		itemUpdate();
		boomUpdate();
	}
	function drawStageObjects() {   
		drawGround();
		drawUser();
		drawMonster();
		drawMissile();
		drawItem();
		drawBoom(); 
	} 
	function gameUpdate(){
		if (t >= 7000) {  
			modal_title="미션 성공"; 
			user_y -= 10;
			if (user_y <= -100) {
				game = false;
			}
		}
	}
	function userUpdate(){
		if (user_shield == t) {
			user_shield = 0;
			userImg.src="/img/user.png";
		}
		if (user_Left==true) {
			if (user_x >= 5) {// 화면에 넘어가지 못하게
				user_x -= 5;
			}
		}
		if (user_Right==true) {
			if (user_x <= 420) {// 화면에 넘어가지 못하게
				user_x += 5;
			}
		}
	}
	function groundUpdate(){
		if(backGround_y1>=800){
			backGround_y1=0;  
			return;   
		} 
		if(backGround_y2>=0){
			backGround_y2=-800;
			return;
		}  
		backGround_y2+=2; 
		backGround_y1+=2;
	} 
	function monsterUpdate(){
		if (t >= 0 && t <= 2000) {
			monster_img = new Image();
			monster_img.src="/img/monster1.png";
			monster_shield = 3;
			monster_speed = 3;
		} else if (t >= 2000 && t <= 4000) {
			monster_img = new Image();
			monster_img.src="/img/monster2.png";
			monster_shield = 5;
			monster_speed = 4;
		} else if (t >= 4000 && t <= 6000) {
			monster_img = new Image();
			monster_img.src="/img/monster3.png";
			monster_shield = 8;   
			monster_speed = 5; 
		}
		for (var i = 0; i < monsterArray.length; i++) {
			var monster = monsterArray[i];
			monster.move();
			if (monster.getY() >= 800) {// 몬스터 화면 밖으로 나가면 삭제
				var length=monsterArray.length;
				monsterArray.splice(length,1);
			}
			if (check(user_x, user_y, userImg, monster.getX(), monster.getY(),
					monster_img)) { 
				if (user_shield == 0) {
					var boom = new Boom(monster.getX(), monster.getY(), t + 50);
					boomArray.push(boom);
					var boom = new Boom(user_x, user_y, t + 50);
					boomArray.push(boom);
					monsterArray.splice(i,1);// 몬스터가 사용자와 부디치면 삭제 
					user_shield = 1;
				} else if (user_shield != 0) {
					var boom = new Boom(monster.getX(), monster.getY(), t + 50);
					boomArray.push(boom);
					
					var rn = Math.floor((Math.random() * 10) + 1);
// System.out.println("아이템 번호--->" + rn);
					switch (rn) {
					case 2:

						var item = new Item(monster.getX(), monster.getY() - 30, 4,
								rn);
						itemArray.push(item);
						break;
					case 3:
						var item = new Item(monster.getX(), monster.getY() - 30, 4,
								rn);
						itemArray.push(item);

						break;
					default:
						var item = new Item(monster.getX(), monster.getY() - 30, 4,
								1);
						itemArray.push(item);

						break;
					}
					monsterArray.splice(i,1);// 몬스터가 사용자와 부디치면 삭제 
				}

			}
		}
		var x = 0;
		if (t % 300 == 0&&t<=6500) {
			for (var i = 0; i < 5; i++) { 
				var monster = new Monster(x, -10, monster_speed, monster_shield);
				monsterArray.push(monster);
				x += 99;
			}  
		} 
	}
	function missileUpdate(){
		for (var i = 0; i < missileArray.length; i++) {
			var missile = missileArray[i];
			missile.move(); 
			if (missile.getY() <= 0) { 
				var length=missileArray.length;
				missileArray.splice(length,1);
			}
			for (var l = 0; l < monsterArray.length; l++) {
				var monster = monsterArray[l];
				if (check(missile.getX(), missile.getY(), missile_img,
						monster.getX(), monster.getY(), monster_img)) {

					if (missileArray.length != 0) {
//System.out.println("삭제되는 i------>" + i);
							missileArray.splice(i,1);
					}  

					monster.setHit(missile_power);
					if (monster.getShield() <= 0) {
						var boom = new Boom(monster.getX(), monster.getY(),
								t + 50);
						boomArray.push(boom);// 폭파 처리 
						// 몬스터가 죽었을때 랜덤으로 아이템 생성
						var rn = Math.floor((Math.random() * 10) + 1);
// System.out.println("아이템 번호--->" + rn);
						switch (rn) {
						case 2:

							var item = new Item(monster.getX(),
									monster.getY() - 30, 4, rn);
							itemArray.push(item);
							break;
						case 3:
							var item = new Item(monster.getX(),
									monster.getY() - 30, 4, rn);
							itemArray.push(item);

							break;
						default:
							var item = new Item(monster.getX(),
									monster.getY() - 30, 4, 1);
							itemArray.push(item);

							break;
						} 
						monsterArray.splice(l,1);
					}

				}

			}  
		}
		if (t % 20 == 0 && t <= 6700) {
			var missile = new Missile(user_x + 12, user_y - 10, 5, missile_power);
			missileArray.push(missile); 
		}
	}
	function itemUpdate(){
		for (var i = 0; i < itemArray.length; i++) {
			var item = itemArray[i];
			item.move();
			if (item.getY() >= 800) {
				itemArray.splice(i,1);
			}
			if (check(user_x, user_y, userImg, item.getX(), item.getY(),
					itemImgArray[item.getType() - 1])) {
				switch (item.getType()) {
				case 2:// 2번 아이템 획득시 미사일 수 증가
					if (missile_power <= 2) {
						missile_power++;
// System.out.println("미사일 수--->" + missile_power);
					} 
					break; 
				case 3:
					userImg.src="/img/user2.png";
					user_shield = t + 800;// 8초간 무적
					break;

				default: 
					score += 10;
					break;
				}  
				itemArray.splice(i,1);
			}
		}
	}
	function boomUpdate(){
		for (var i = 0; i < boomArray.length; i++) {
			var boom = boomArray[i];

			if (boom.getTime() <= t) {
				// 폭파 시간 끝나면 폭파 이미지 제거
				boomArray.splice(i,1);
			}
		}
	}
	
	function drawGround(){
		context2.drawImage(groundArray[0],0,backGround_y1); 
	    context2.drawImage(groundArray[1],0,backGround_y2); 
	    context2.drawImage(score_img,10,10); 
	    context2.fillStyle = "#FFFFFF"; 
	    context2.font = "20px arial";        
	    context2.fillText(""+score,150,35);
//	    context2.drawString(""+score , 130, 50);
	    
	}  
	function drawUser(){
		if(user_shield==1){
			return; 
		}
	    context2.drawImage(userImg,user_x,user_y);
	}
	function drawMonster(){
		for (var i = 0; i < monsterArray.length; i++) {
			var monster = monsterArray[i]; 
			context2.drawImage(monster_img, monster.getX(), monster.getY());
		}
	}
	function drawMissile(){
		for (var i = 0; i < missileArray.length; i++) {
			var missile = missileArray[i];
			if (missile_power == 1) {
				context2.drawImage(missile_img, missile.getX(), missile.getY());
			} else if (missile_power == 2) {
				context2.drawImage(missile_img, missile.getX(), missile.getY());
				context2.drawImage(missile_img, missile.getX() - 10,
						missile.getY());
			} else if (missile_power == 3) {

				context2.drawImage(missile_img, missile.getX() - 12,
						missile.getY());
				context2.drawImage(missile_img, missile.getX() + 12,
						missile.getY());
				context2.drawImage(missile_img, missile.getX(),
						missile.getY() - 5);

			}
		}
	}
	function drawItem(){
		for (var i = 0; i < itemArray.length; i++) {
			var item = itemArray[i];
			context2.drawImage(itemImgArray[item.getType() - 1], item.getX(),
					item.getY());
		}
	}
	function drawBoom(){
		for (var i = 0; i < boomArray.length; i++) {
			var boom = boomArray[i];
			if (boom.getTime() - 25 >= t) {
				context2.drawImage(boomImgArray[0], boom.getX(), boom.getY());
			} else if (boom.getTime() >= t) {
				context2.drawImage(boomImgArray[1], boom.getX(), boom.getY());
			}
		}  
	}
	
	function check(x1,y1,img1,x2,y2,img2) {
		var bool; 
		if(img1.width==null){
			img1.width=-1;
		}
		if(img1.height==null){
			img1.height=-1;
		}
		if(img2.width==null){
			img2.width=-1;
		}
		if(img2.height==null){
			img2.height=-1;
		} 
		if (Math.abs((x1 + img1.width / 2)
				- (x2 + img2.width / 2)) < (img2.width / 2 + img1
				.width / 2)
				&& Math.abs((y1 + img1.height / 2)
						- (y2 + img2.height / 2)) < (img2
						.height / 2 + img1.height / 2)) {
			// 이미지 넓이, 높이값을 바로 받아 계산합니다.
			bool = true;
		} else {
			bool = false;
		}
		return bool;
	}
	
	  init(); 
}); 