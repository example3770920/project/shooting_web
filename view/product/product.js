'use strict';
angular.module('myApp.product', [ 'ngRoute' ])
.config([ '$routeProvider', function($routeProvider) {
	$routeProvider.when('/product', {
	templateUrl : 'view/product/product.html',
	});
}])
.directive('onLastRepeat', function() {
return function(scope, element, attrs) {
    if (scope.$last) setTimeout(function(){
        scope.$emit('onRepeatLast', element, attrs);
        }, 1);
    }; 
})
.controller('productCtrl',function($scope, $http) {
	initSlide();  
	$("a[href$='http://amazingslider.com']").parent().remove();
	$("img[src$='http://amazingslider.com/wp-content/uploads/amazingslider/15/sliderengine/skins/bottomshadow-110-95-3.png']").remove();
 
	$scope.initModalCloseEvent=function(){
		$('#myModal').on('hidden.bs.modal', function () {
			$("#name").val("");
			$("#type").val("0");
			$("#productFunction").val("");
			$("#warranty").val("");
			$("#price").val("");
			$("#paperQty").val("");
			$("#contractDate").val("");
			$("#imgFile").val("");
			$("#basicContent").val("");
			$("#content").val("");   
			var optionValue = $('input:radio[name="option"]:checked').val();
			if (optionValue == '1') {
				for(var i=0;i<$scope.optionCount;i++){
					$("#contractDate"+i).remove();
					$("#price"+i).remove(); 
					$("#delBtn"+i).remove(); 
					$("#deleteCheck"+i).remove(); 
					
				}
			}
			if (optionValue == '2') {      
				for(var i=0;i<$scope.optionCount;i++){
					$("#paperQty"+i).remove();
					$("#price"+i).remove(); 
					$("#delBtn"+i).remove(); 
					$("#deleteCheck"+i).remove(); 
					
				}
			} 
			$scope.optionCount=0;
			$('input:radio[name=option]:input[value=1]').attr("checked", true);
			
		});
		$('#updateModal').on('hidden.bs.modal', function () {
			//$("#up_paperQty_").val("");
			//$("#up_contractDate_").val("");
			var optionValue = $('input:radio[name="up_option"]:checked').val();
			if (optionValue == '1') {
				for(var i=0;i<$scope.optionCount;i++){
					$("#up_contractDate"+i).remove();
					$("#up_price"+i).remove(); 
					$("#delBtn"+i).remove(); 
					$("#deleteCheck"+i).remove(); 
					$("#up_sn"+i).remove(); 
					
					
				} 
			} 
			if (optionValue == '2') {      
				for(var i=0;i<$scope.optionCount;i++){
					$("#up_paperQty"+i).remove();
					$("#up_price"+i).remove(); 
					$("#delBtn"+i).remove();  
					$("#deleteCheck"+i).remove(); 
					$("#up_sn"+i).remove(); 
					
				}
			} 
			$scope.optionCount=0; 
		});
	} 
	$scope.initModalCloseEvent();
    $scope.$on('onRepeatLast', function(scope, element, attrs){
		$(".productDeleteBtn").each(function(){
			$(this).hover(function(){
				$(this).attr({src: "image/btn_x_roc.png"});
			},function(){
				$(this).attr({src: "image/btn_x.png"});
			});
		});
		$(".productUpdateBtn").each(function(){
			$(this).hover(function(){
				$(this).attr({src: "image/btn_edit_roc.png"});
			},function(){ 
				$(this).attr({src: "image/btn_edit.png"});
			});
		});
		
		 
		
    });
 	$scope.oEditors = [];
	$scope.createEditor = function(name) {
		$("iframe").remove();
		var sSkinUri = "naverSmart/SmartEditor2Skin.html";
		nhn.husky.EZCreator.createInIFrame({
			oAppRef : $scope.oEditors,
			elPlaceHolder : name,  
			sSkinURI : "naverSmart/SmartEditor2Skin.html",
			fOnAppLoad : function() {
				$scope.textAreaMapping(name);
			},
			fCreator : "createSEditor2",
		});   
	}
	$scope.textAreaMapping = function(name) {
		if (name == 'content') {
			return;
		}
		if (name == 'up_content') {
			$scope.oEditors.getById["up_content"].exec(
					"PASTE_HTML", [ $scope.content ]);
			return;
		}
		if (name == 'info_content') {
			$scope.oEditors.getById["info_content"].exec(
					"PASTE_HTML", [ $scope.content ]);
			return;
		}
	}
	$scope.createProductList = function() {
		var method = 'POST';
		var url = 'act/productList.php';
		$http({
			method : method,
			url : url,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			},
		}).success(function(data) {
			$scope.productList = data;
			$scope.arrangeImageText();
		}).error(function(response) {  
		});
	} 
	$scope.setInputMaskMoney = function(id) {
		$(id).maskMoney({
			prefix : ' ', // The symbol to be displayed before
						 	// the value entered by the user
			allowZero : false, // Prevent users from inputing
								// zero
			allowNegative : true, // Prevent users from
									// inputing negative values
			defaultZero : false, // when the user enters the
									// field, it sets a default
									// mask using zero
			thousands : ',', // The thousands separator
			decimal : ',', // The decimal separator
			precision : 3, // How many decimal places are
							// allowed
			affixesStay : false, // set if the symbol will
									// stay in the field after
									// the user exits the field.
			symbolPosition : 'left' // use this setting to
									// position the symbol at
									// the left or right side of
									// the value. default 'left'
		}); //  
	}
	$scope.productInsert = function() { 
		// 에디터의 내용이 textarea에 적용된다.
		$scope.oEditors.getById["content"].exec("UPDATE_CONTENTS_FIELD", []);
		if ($scope.formDataCheck()) { 
			var optionValue = $('input:radio[name="option"]:checked').val();
			if (optionValue == 1) {
				$("#paperQtyDiv").remove();
			} 
			if (optionValue == 2) {
				$("#contractDateDiv").remove();
			} 
			document.insertForm.submit();
		}; 
	}; 
	$scope.moneyToNumber=function(id){
		var moneyStr = $(id).val().split(",");
		var money = "";
		for ( var i = 0; i < moneyStr.length; i++) {
			money += moneyStr[i]; 
		} 
		$(id).val(money);
	}
	$scope.formDataCheck = function() {
		if ($("#name").val().length <= 0) {
			alert("상품명을 입력하세요.");
			return false;
		}
		if ($("#productFunction").val().length <= 0) {
			alert("상품기능을 입력하세요.");
			return false;
		}
		if ($("#warranty").val().length <= 0) {
			alert("보증료 입력하세요.");
			return false;
		}
		if ($("#price").val().length <= 0) {
			alert("상품가격을 입력하세요.");
			return false;
		}
		if ($("#price").val().length > 0) {
			var money_check=/^[^a-zA-Zㄱ-ㅎ가-힣]*$/; 
			if($("#price").val().search(money_check)!=-1){
			}else{ 
				alert("상품가격을 숫자만 입력하세요.");
				return false;
			}
		}
		for(var i = 0;i<$scope.optionCount;i++){
			if($("#deleteCheck"+i).val()==1){
				continue;
			}
			if ($("#price"+i).val().length > 0) {
				var money_check=/^[^a-zA-Zㄱ-ㅎ가-힣]*$/; 
				if($("#price"+i).val().search(money_check)!=-1){
				}else{ 
					alert("금액은 숫자만 입력하세요.");
					$("#price"+i).focus();
					return false;
				}
			}
		}
		if ($("#imgFile").val().length <= 0) {
			alert("상품이미지 선택하세요.");
			return false;
		} else {
			var imgArray = $("#imgFile").val().split(".");
			if (imgArray[1] != 'jpg' && imgArray[1] != 'png') {
				alert("이미지는 jpg또는png 파일만 가능합니다");
				return false;
			}
		} 
		if ($("#basicContent").val().length <= 0) {
			alert("상품간단 설명을 입력하세요.");
			return false;
		}
		if ($("#content").val().length <= 0) {
			alert("상품상세 설명을 입력하세요.");
			return false;
		}
		return true;
	}
	$scope.arrangeImageText = function() {
		var isImgPosLeft = true;
		var imgPosCount = 0;
		for (var i = 0; i < $scope.productList.length; ++i) {
			var product = $scope.productList[i];
			product.imageLeft = isImgPosLeft;
			product.index = i;
			++imgPosCount;
			if (2 == imgPosCount) {
				imgPosCount = 0;
				isImgPosLeft = !isImgPosLeft;
			}
		}

	}
	$scope.productAddOpen = function() {
		var id = "#price"; 
		var option=document.getElementsByName("option");
		option[0].checked=true;
		option[1].checked=false;
		$scope.setInputMaskMoney(id);
		$scope.contractDateDivShow(); 
		$('#myModal').modal('show'); 
		$scope.createEditor('content');
		
	}  

	$scope.adminLoginCheck = function() {
		var method = 'POST';
		var url = 'act/adminLoginCheck.php';
		$http(
				{
					method : method,
					url : url,
					headers : {
						'Content-Type' : 'application/x-www-form-urlencoded'
					},
				}).success(function(data) {
			if (data == true) {
				$("#addBtn").hover(function(){  
					$("#addBtn").attr({src: "image/btn_plus_roc.png"});
				},function(){  
					$("#addBtn").attr({src: "image/btn_plus.png"});
				});   
				$scope.sortableStart();
				return;
			}
		}).error(function(response) {
			alert("error");
		});
	}
	$scope.productDelete = function(sn) {
		var msg = "해당 상품을 삭제 하시겠습니까?"
		if (!confirm(msg)) {
			return false;
		} else {
			var url = "act/productDelete.php?pSn=" + sn;
			$http.get(url).success(function(data) {
				if (data != 1) {
					alert("상품 삭제 실패.");
				}
				location.href = "index.php";
			}).error(function(response) {
				alert("error");
			});
		}

	}
	$scope.productInfoForm = function(sn) {
		var url = "act/productInfo.php?pSn=" + sn;

		$http.get(url).success(function(data) {
			$('#infoModal').modal('show');
			$scope.productInfo=data[0];
			$scope.productOptionInfo=data[1];
			
			$scope.productInfoFormMapping(); 
		}).error(function(response) {
			alert("error");
		}); 
	}
	$scope.productInfoFormMapping = function() { 
		$("#info_imgFile").attr({src:"image/product/"+$scope.productInfo.imageFileName});
		$("#info_textArea").html($scope.productInfo.content);
		if($scope.productInfo.optionType==1){
			$("#contractDateSelect").change(function(){
				var resultPrice=$scope.productResultPrice($("#contractDateSelect").val());
				$("#resultPrice").html(resultPrice);  
			});    
		}  
		if($scope.productInfo.optionType==2){
			$("#paperQtySelect").change(function(){
				var resultPrice=$scope.productResultPrice($("#paperQtySelect").val());
				$("#resultPrice").html(resultPrice);    
			});  
		} 
	} 
	$scope.productResultPrice=function(selectedOption){
		var basicPrice=$scope.productInfo.price.replace(",","");
		var optionPrice=selectedOption.replace(",",""); 
		var result=Number(basicPrice)+Number(optionPrice);
		return formatnumber(result,"3");
	}  
	$scope.productUpdateForm = function(sn) {
		var url = "act/productUpdateForm.php?pSn=" + sn;
		$http.get(url).success(function(data) {
			if (data == 'ERROR') {
				return false;
			}
			$scope.productFormData=data;
			$scope.productFormMapping(data); 
			$scope.content = data[0].content;
			$('#updateModal').modal('show');
			var id = "#up_price";
			$scope.setInputMaskMoney(id);
			$scope.createEditor('up_content');
		}).error(function(response) {
			alert("error");
		});
	}
	$scope.productUpdate = function() {
		$scope.oEditors.getById["up_content"].exec(
				"UPDATE_CONTENTS_FIELD", []);
		if ($scope.updateFormDataCheck()) {
			var optionValue = $('input:radio[name="up_option"]:checked').val();
			if (optionValue == 1) {
				$("#up_paperQtyDiv").remove();
			} 
			if (optionValue == 2) {
				$("#up_contractDateDiv").remove();
			}
			document.updateForm.submit();
		};
	}
	$scope.updateFormDataCheck = function() {
		if ($("#up_name").val().length <= 0) {
			alert("상품명을 입력하세요.");
			return false;
		} 
		if ($("#up_productFunction").val().length <= 0) {
			alert("상품기능을 입력하세요.");
			return false;
		}
		if ($("#up_warranty").val().length <= 0) {
			alert("보증료 입력하세요.");
			return false;
		}
		if ($("#up_price").val().length <= 0) {
			alert("상품가격을 입력하세요.");
			return false;
		}
		if ($("#up_price").val().length > 0) {
			var money_check=/^[^a-zA-Zㄱ-ㅎ가-힣]*$/; 
			if($("#up_price").val().search(money_check)!=-1){
			}else{ 
				alert("상품가격을 숫자만 입력하세요.");
				return false;
			}
		}
		for(var i = 0;i<$scope.optionCount;i++){
			if($("#deleteCheck"+i).val()==1){
				continue;
			}
			  
			if ($("#up_price"+i).val().length > 0) {
				var money_check=/^[^a-zA-Zㄱ-ㅎ가-힣]*$/; 
				if($("#up_price"+i).val().search(money_check)!=-1){
				}else{ 
					alert("숫자만 입력하세요.");
					$("#up_price"+i).focus();
					return false;
				}
			}
		}
		if ($("#up_imgFile").val().length > 0) {
			var imgArray = $("#up_imgFile").val().split(".");
			if (imgArray[1] != 'jpg' && imgArray[1] != 'png') {
				alert("이미지는 jpg또는png 파일만 가능합니다");
				return false;
			}
		}
		if ($("#up_basicContent").val().length <= 0) {
			alert("상품간단 설명을 입력하세요.");
			return false;
		}
		if ($("#up_content").val().length <= 0) {
			alert("상품상세 설명을 입력하세요.");
			return false;
		} 
		return true;
	}
	$scope.productFormMapping = function(data) {
		$("#up_type").val(data[0].type);   
		$("#up_imgSrc").attr({src: "image/product/"+data[0].imageFileName});
		 
		var option=document.getElementsByName("up_option");
		if(data[0].optionType==1){
			option[0].checked=true;
			option[1].checked=false;
		}
		if(data[0].optionType==2){
			option[0].checked=false;
			option[1].checked=true;
		} 
		var optionValue = $('input:radio[name="up_option"]:checked').val();
		if(optionValue==1){
			$("#up_contractDateDiv").show();
			$("#up_paperQtyDiv").hide();
			for(var i=0;i<data[1].length;i++){ 
				if(data[1][i].type==2){
					$("#up_paperQty_").val(data[1][i].content);
					$("#up_paperQtySn").val(data[1][i].sn);
					continue;
				}
				$scope.optionUpdateAppend(data[1][i].content,data[1][i].addPrice,data[1][i].sn);
			}
		}
		if(optionValue==2){
			$("#up_paperQtyDiv").show();
			$("#up_contractDateDiv").hide();
			for(var i=0;i<data[1].length;i++){ 
				if(data[1][i].type==1){   
					$("#up_contractDate_").val(data[1][i].content);
					$("#up_contractDateSn").val(data[1][i].sn);
					continue;
				}  
				$scope.optionUpdateAppend(data[1][i].content,data[1][i].addPrice,data[1][i].sn);
			}
		}
	}
	$scope.up_contractDateDivShow = function() {
		$("#up_contractDateDiv").show();
		$("#up_paperQtyDiv").hide();
		$("#up_contractDate_").val("");
		$("#up_paperQtyDiv .form-controlCustom").each(function() {
			$(this).remove();
		});
		$scope.optionCount = 0;
		$scope.optionUpdateAppend('','','');
	}
	$scope.up_paperQtyDivShow = function() {
		$("#up_paperQtyDiv").show();
		$("#up_contractDateDiv").hide();
		$("#up_paperQty_").val(""); 
		$("#up_contractDateDiv .form-controlCustom").each(
				function() {
					$(this).remove();
				});
		$scope.optionCount = 0;
		$scope.optionUpdateAppend('','','');
	}
	$scope.sortableStart = function() {
		$("#product_list_wrap")
				.sortable(
						{
							connectWith : ".connectedSortable",
							start : function(event, ui) {
								$scope.change1Position = ui.item
										.index(".product_wrap");
								$scope.snArray = $scope
										.snArrayInit();
								$scope.noArray = $scope
										.noArrayInit();
								$scope.change1PositionInfo = $scope
										.createPositionInfo($scope.change1Position);
							},
							change : function(event, ui) { 
							},
							update : function(event, ui) {
								$scope.change2Position = ui.item
										.index(".product_wrap");
								$scope.change2PositionInfo = $scope
										.createPositionInfo($scope.change2Position);

								if (!confirm("해당 상품 위치를 변경 하시겠습니까?")) {
									return false;
								} else {
									$scope.updateProductPosition();
								}
							}
						}).disableSelection();

	}
	$scope.snArrayInit = function() {
		var snArray = new Array();
		$('input.sn').each(function(idx) {
			snArray[idx] = $(this).val();
		});

		return snArray;
	}
	$scope.noArrayInit = function() {
		var noArray = new Array();
		$('input.no').each(function(idx) {
			noArray[idx] = $(this).val();
		});

		return noArray;
	}
	$scope.createPositionInfo = function(idx) {
		var data = {
			no : $scope.noArray[idx],
			sn : $scope.snArray[idx]
		}
		return data;
	}
	$scope.updateProductPosition = function() {
		var data = {
			no1 : $scope.change1PositionInfo.no,
			no2 : $scope.change2PositionInfo.no,
		} 
		$http(
				{
					method : 'POST',
					url : "act/productPositionUpdate.php",
					data : data,
					headers : {
						'Content-Type' : 'application/x-www-form-urlencoded'
					}
				}).success(function(data) {
			if (data==false) {  
				alert("변경 실패");
			}
			location.href = "index.php";
		}).error(function(response) {
			alert("error");
		});

	}

	$scope.contractDateDivShow = function() {
		$("#contractDateDiv").show();
		$("#paperQtyDiv").hide();
		$("#contractDate").val("");
		$("#paperQtyDiv .form-controlCustom").each(function() {
			$(this).remove();
		});
		$scope.optionCount = 0;
		$scope.optionAppend();
	}
	$scope.paperQtyDivShow = function() {
		$("#paperQtyDiv").show();
		$("#contractDateDiv").hide();
		$("#paperQty").val(""); 
		$("#contractDateDiv .form-controlCustom").each(
				function() {
					$(this).remove();
				});
		$scope.optionCount = 0;
		$scope.optionAppend();
	} 
	$scope.optionCount=0; 
	$scope.optionUpdateAppend = function(content,price,sn) {
		
		var optionValue = $('input:radio[name="up_option"]:checked').val();
		if (optionValue == '1') { 
			$("#up_contractDateDiv").append("<input type='text' class='form-controlCustom' id='up_contractDate"+$scope.optionCount + "' name='up_contractDate[]' placeholder='Enter ContractDate' value='"+content+"'>");
			$("#up_contractDateDiv").append("<input type='text' class='form-controlCustom' id='up_price"+ $scope.optionCount + "'name='up_addPrice[]'  placeholder='Enter price' value='"+price+"'>");
			$("#up_contractDateDiv").append("<input type='hidden' name='optionSn[]' id='up_sn"+$scope.optionCount+"' value='"+sn+"'>");
			$("#up_contractDateDiv").append("<input type='hidden' name='deleteCheck[]' id='deleteCheck"+$scope.optionCount+"' value=0>");
			
			$("#up_contractDateDiv").append("<button type='button' class='close' id='delBtn"+$scope.optionCount+"' onclick=deleteOption('up_contractDateDiv',"+$scope.optionCount+")>x</button>");
		}     
		if (optionValue == '2') {  
			$("#up_paperQtyDiv").append("<input type='text' class='form-controlCustom' id='up_paperQty"+$scope.optionCount + "' name='up_paperQty[]'  placeholder='Enter PaperQty' value='"+content+"'>");
			$("#up_paperQtyDiv").append("<input type='text' class='form-controlCustom' id='up_price"+ $scope.optionCount+ "'name='up_addPrice[]'  placeholder='Enter price' value='"+price+"'>");
			$("#up_paperQtyDiv").append("<input type='hidden' name='optionSn[]' id='up_sn"+$scope.optionCount+"' value='"+sn+"'>");
			$("#up_paperQtyDiv").append("<button type='button' class='close' id='delBtn"+$scope.optionCount+"'onclick=deleteOption('up_paperQtyDiv',"+$scope.optionCount+")>x</button>");
			$("#up_paperQtyDiv").append("<input type='hidden' name='deleteCheck[]' id='deleteCheck"+$scope.optionCount+"' value=0>");
			 
		}     
		 
		var id = "#up_price" + $scope.optionCount
		$scope.setInputMaskMoney(id);  
		$scope.optionCount = $scope.optionCount + 1;
	}
	$scope.contractDateSelectRemove=function(type){
		if(type==2){
			$("#contractDateSelect option:last").remove();
		} 
	}  
	$scope.paperQtySelectRemove=function(type){
		if(type==1){
			$("#paperQtySelect option:last").remove();
		}
	}
	$scope.optionAppend = function() {
		var optionValue = $('input:radio[name="option"]:checked').val();
		if (optionValue == '1') { 
			$("#contractDateDiv").append("<input type='text' class='form-controlCustom' id='contractDate"+ $scope.optionCount+ "'name='contractDate[]' placeholder='Enter ContractDate'>");
			$("#contractDateDiv").append("<input type='text' class='form-controlCustom' id='price"+ $scope.optionCount+ "'name='addPrice[]'  placeholder='Enter price' >");
			$("#contractDateDiv").append("<button type='button' class='close' id='delBtn"+$scope.optionCount+"'onclick=deleteOption('contractDateDiv',"+$scope.optionCount+")>x</button>");
			$("#contractDateDiv").append("<input type='hidden' name='deleteCheck[]' id='deleteCheck"+$scope.optionCount+"' value=0>");
		}   
		if (optionValue == '2') {   
			$("#paperQtyDiv").append("<input type='text' class='form-controlCustom' id='paperQty"+ $scope.optionCount+ "' name='paperQty[]'  placeholder='Enter PaperQty' >");
			$("#paperQtyDiv").append("<input type='text' class='form-controlCustom' id='price"+ $scope.optionCount+ "'name='addPrice[]'  placeholder='Enter price' >");
			$("#paperQtyDiv").append("<button type='button' class='close' id='delBtn"+$scope.optionCount+"'onclick=deleteOption('paperQtyDiv',"+$scope.optionCount+")>x</button>");
			$("#paperQtyDiv").append("<input type='hidden' name='deleteCheck[]' id='deleteCheck"+$scope.optionCount+"' value=0>");
		} 
		
		var id = "#price" + $scope.optionCount;
		$scope.setInputMaskMoney(id);  
		$scope.optionCount = $scope.optionCount + 1;
	} 
	
	$scope.initInsertFormRadioChangeEvent=function(){
		$("input:radio[name=option]").change(function(){
			var checked=$(':radio[name="option"]:checked').val();
			if (!confirm("변경시 작성하신 내용이 삭제됩니다.")) {
				if(checked==1){
					var option=document.getElementsByName("option");
					option[0].checked=false;
					option[1].checked=true;
					return false;
				} 
				if(checked==2){
					var option=document.getElementsByName("option");
					option[0].checked=true;
					option[1].checked=false;
					return false;
				} 
			} 
			if(checked==1){
				$scope.contractDateDivShow();
			}
			if(checked==2){
				$scope.paperQtyDivShow();
			}
		}); 
		
	} 
	
	$scope.initUpdateFormRadioChangeEvent=function(){
		$("input:radio[name=up_option]").change(function(){
			var checked=$(':radio[name="up_option"]:checked').val();
			if (!confirm("변경시 작성하신 내용이 삭제됩니다.")) {
				if(checked==1){
					var option=document.getElementsByName("up_option");
					option[0].checked=false;
					option[1].checked=true;
					return false;
				} 
				if(checked==2){
					var option=document.getElementsByName("up_option");
					option[0].checked=true;
					option[1].checked=false;
					return false;
				} 
			} 
			if(checked==1){
				$scope.up_contractDateDivShow();
			}
			if(checked==2){
				$scope.up_paperQtyDivShow();
			}
		}); 
	}
	$scope.adminLoginCheck();
	$scope.createProductList();  
	$scope.initInsertFormRadioChangeEvent(); 
	$scope.initUpdateFormRadioChangeEvent(); 
});
function deleteOption(option,optionIndex){
	if(option=='paperQtyDiv'){
		$("#paperQty"+optionIndex).hide();
		$("#price"+optionIndex).hide();
		$("#delBtn"+optionIndex).hide();
		$("#deleteCheck"+optionIndex).val("1");
		
		return false;
	} 
	if(option=='contractDateDiv'){
		
		$("#contractDate"+optionIndex).hide(); 
		$("#price"+optionIndex).hide();
		$("#delBtn"+optionIndex).hide();
		$("#deleteCheck"+optionIndex).val("1");
		
		return false;
	}
	if(option=='up_paperQtyDiv'){
		
		$("#up_paperQty"+optionIndex).hide();
		$("#up_price"+optionIndex).hide();
		$("#up_sn"+optionIndex).hide();
		$("#delBtn"+optionIndex).hide();
		$("#deleteCheck"+optionIndex).val("1");
		
		return false;
	}
	if(option=='up_contractDateDiv'){ 
		
		$("#up_contractDate"+optionIndex).hide();
		$("#up_price"+optionIndex).hide();
		$("#up_sn"+optionIndex).hide();
		$("#delBtn"+optionIndex).hide();
		$("#deleteCheck"+optionIndex).val("1");
		
		return false;
	}
} 
