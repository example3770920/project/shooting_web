'use strict';

angular.module('myApp.contacts', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/contacts', {
	templateUrl: 'view/contacts/contacts.html',
  });
}]) 
.controller('contactsCtrl', function($scope,$http) {
	
	initMap();
	
});

function initMap() {   
		
	var geocoder = new daum.maps.services.Geocoder();  
	// 주소로 좌표를 검색합니다
	var addrss="서울특별시 영등포구 당산동 5가 33-1 한강포스빌";
	geocoder.addr2coord(addrss, function(status, result) {
	    // 정상적으로 검색이 완료됐으면 
	     if (status === daum.maps.services.Status.OK) {
			var coords = new daum.maps.LatLng(result.addr[0].lat, result.addr[0].lng);
			var mapContainer = document.getElementById('contacts_map'); // 지도를 표시할 div 
			var mapOption = {
				center: coords, // 지도의 중심좌표
				level: 3 // 지도의 확대 레벨 
			};  
			// 지도를 생성합니다    
			var map = new daum.maps.Map(mapContainer, mapOption); 

	        // 결과값으로 받은 위치를 마커로 표시합니다
	        var marker = new daum.maps.Marker({
	            map: map,
	            position: coords
	        });

	        // 인포윈도우로 장소에 대한 설명을 표시합니다
	       var infowindow = new daum.maps.InfoWindow({
	            content: '<div style="padding:5px;">우리집</div>'
	        }); 
			    
	      infowindow.open(map, marker); 
			map.center = coords;  
			return; 
	    } 
	    if(status==daum.maps.services.Status.ZERO_RESULT){
	    	//정상적으로 응답은 받았으나 결과값이 없을때
	    	alert("정확한 주소를 기입해 주세요.");
 	    	return;
	    }
	    if(status==daum.maps.services.Status.ERROR){
	    	//서버 응답에 문제가 있는경우
 	    	alert("지도를 불러오는데 서버에 문제가 있습니다.");
	    	return;
	    }
	});
	}